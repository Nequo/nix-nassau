# Nix-Nassau

These are my WSL configuration files, managed with [home-manager](https://github.com/nix-community/home-manager).

## Bootstrap

To get started, install the Ubuntu WSL distro from the microsoft store.

Update and install dependancies:

```shell
sudo apt-get update
sudo apt-get install curl xz-utils ca-certificates
```

Install nix and [home-manager](https://nix-community.github.io/home-manager/index.html#sec-install-standalone):

```shell
curl -L https://nixos.org/nix/install | sh
source $HOME/.nix-profile/etc/profile.d/nix.sh
export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install
```


## References

- https://alexpearce.me/2021/07/managing-dotfiles-with-nix/
- https://www.kunxi.org/2020/11/nix-on-wsl/
- https://gutier.io/post/development-using-rust-with-nix/
