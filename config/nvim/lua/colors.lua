-- PaleFire
-- local M = {
--  base00 = "#1E1E1E",
--  base01 = "#2E2E2E",
--  base02 = "#383838",
--  base03 = "#9fafaf",
--  base04 = "#444444",
--  base05 = "#585858",
--  base06 = "#6c6c6c",
--  base07 = "#EAEAD3",
--  red    = "#B37576",
--  orange = "#ffaf87",
--  yellow = "#F1E4A8",
--  green  = "#8DB98D",
--  cyan   = "#6CBBBF",
--  blue   = "#92C4EC",
--  purple = "#DC8CC3",
--  brown  = "#B9AC72",
--  white  = "#ffffff",
--  fg = '#EAEAD3'
--}
-- OceanicNext
-- local M = {
--   base00 = "#1b2b34",
--   base01 = "#343d46",
--   base02 = "#4f5b66",
--   base03 = "#65737e",
--   base04 = "#a7adba",
--   base05 = "#c0c5ce",
--   base06 = "#cdd3de",
--   base07 = "#d8dee9",
--   red = "#ec5f67",
--   orange = "#f99157",
--   yellow = "#fac863",
--   green = "#99c794",
--   cyan = "#62b3b2",
--   blue = "#6699cc",
--   purple = "#c594c5",
--   brown = "#ab7967",
--   white = "#ffffff"
-- }
-- Tomorrow night
local M = {
  base00 = "#1d1f21",
  base01 = "#282a2e",
  base02 = "#373b41",
  base03 = "#969896",
  base04 = "#b4b7b4",
  base05 = "#c5c8c6",
  base06 = "#e0e0e0",
  white  = "#ffffff",
  red    = "#cc6666",
  orange = "#de935f",
  yellow = "#f0c674",
  green  = "#b5bd68",
  cyan   = "#8abeb7",
  blue   = "#81a2be",
  purple = "#b294bb",
  brown  = "#a3685a"
}


function M.setItalics()
  vim.cmd("hi Comment gui=italic")
end

return M
