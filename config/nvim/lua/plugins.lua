-- Bootstrap packer
local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end


vim.cmd [[packadd packer.nvim]]
return require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use {'Nequo/doom-one.nvim', branch = 'personal'}
    use 'norcalli/nvim-colorizer.lua'
    use 'mhartington/oceanic-next'
    use { "briones-gabriel/darcula-solid.nvim", requires = "rktjmp/lush.nvim" }
    use 'folke/tokyonight.nvim'
    use 'fenetikm/falcon'
    use "projekt0n/github-nvim-theme"
    use "RRethy/nvim-base16"
    use 'EdenEast/nightfox.nvim'

    use 'tpope/vim-surround'
    use 'guns/vim-sexp'
    use 'tpope/vim-sexp-mappings-for-regular-people'

    use 'hashivim/vim-terraform'
    use 'ekalinin/Dockerfile.vim'

    use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
    use 'nvim-treesitter/playground'

    use 'Olical/conjure'
    use 'tpope/vim-dispatch'
    use 'clojure-vim/vim-jack-in'
    use 'radenling/vim-dispatch-neovim'
    use 'neovim/nvim-lspconfig'
    use 'kosayoda/nvim-lightbulb'
    use 'folke/lsp-trouble.nvim'


    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/nvim-cmp'
    use 'L3MON4D3/LuaSnip'

    use {
        'nvim-telescope/telescope.nvim',
        requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
    }
    use 'kyazdani42/nvim-web-devicons'

    --use 'sindrets/diffview.nvim'
    use 'glepnir/dashboard-nvim'

    use {
        'kyazdani42/nvim-tree.lua',
        requires = 'kyazdani42/nvim-web-devicons',
    }

    use {
        'hoob3rt/lualine.nvim',
        requires = {'kyazdani42/nvim-web-devicons', opt = true}
    }
end)
