{ config, pkgs, ... }:
{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home = {
    username = "nequo";
    homeDirectory = "/home/nequo";
    packages = with pkgs; [
      neovim
      ripgrep
      fzf
    ];
    sessionVariables = {
      EDITOR = "nvim";
    };
  };
  #home.file = { 
  #  "git/pale-fire".source = builtins.fetchGit {
  #    url = "git@github.com:Nequo/palefire-nvim.git";
  #    rev = "96a2958d284a77d731d8cff3acb5603fbf7cbfff";
  #    ref = "main";
  #    #sha256 = "01qn9iqpiq6xpfdh6raqd14yf32f7rhk7wpx0dn4r2d4syah67yf";
  #  };
  #};
  programs.bash = {
    enable = true;
    shellOptions = [];
    historyControl = [ "ignoredups" "ignorespace" ];
    initExtra = builtins.readFile ./config/bashrc;

    shellAliases = {
      vim = "nvim";
      ga = "git add";
      gc = "git commit";
      gco = "git checkout";
      gcp = "git cherry-pick";
      gdiff = "git diff";
      gl = "git prettylog";
      gp = "git push";
      gs = "git status";
      gt = "git tag";
    };
  };
  programs.bat = {
    enable = true;
    config = {
      theme = "zenburn";
      italic-text = "always";
    };
  };
  programs.git = {
    enable = true;
    userName = "Nadim Edde Gomez";
    userEmail = "nadim@eddegomez.org";
    aliases = {
      prettylog = "...";
    };
    extraConfig = {
      core = {
        editor = "nvim";
      };
      color = {
        ui = true;
      };
      push = {
        default = "simple";
      };
      pull = {
        ff = "only";
      };
      init = {
        defaultBranch = "main";
      };
    };
    ignores = [
      ".DS_Store"
      "*.pyc"
    ];
  };
  programs.starship = {
    enable = true;
    enableBashIntegration = true;
    # Configuration written to ~/.config/starship.toml
    settings = {
      # add_newline = false;

      character = {
          success_symbol = "[ﬦ](bold green)";
          error_symbol = "[ﬦ](bold red)";
          vicmd_symbol = "[ﬦ](bold green)";
          disabled = false;
      };
    };
  };

  xdg.configFile.nvim = {
    source = ./config/nvim;
    recursive = true;
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
